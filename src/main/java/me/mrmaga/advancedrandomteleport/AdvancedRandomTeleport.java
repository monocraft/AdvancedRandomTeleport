package me.mrmaga.advancedrandomteleport;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import me.mrmaga.advancedrandomteleport.managers.CooldownManager;
import me.mrmaga.advancedrandomteleport.managers.TeleportManager;
import me.mrmaga.advancedrandomteleport.yml.LanguageConfig;
import me.mrmaga.advancedrandomteleport.yml.MainConfig;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class AdvancedRandomTeleport extends JavaPlugin {

    private MainConfig config;
    private LanguageConfig lang;
    private TeleportManager tm;
    private CooldownManager cm;

    @Override
    public void onEnable() {
        this.config = new MainConfig(this);
        this.lang = new LanguageConfig(this);
        this.tm = new TeleportManager(this);
        this.cm = new CooldownManager(this);

        Commands commands = new Commands(this);
        getCommand("randomteleport").setExecutor(commands);

        String worldName = this.config.getWorldName();
        World world = this.getServer().getWorld(worldName);

        if (null == world) {
            WorldCreator worldCreator = new WorldCreator(worldName);
            worldCreator.environment(World.Environment.NORMAL);
            worldCreator.createWorld();
        }
    }

    public LanguageConfig getLanguageConfig() {
        return lang;
    }

    public MainConfig getMainConfig() {
        return config;
    }

    public TeleportManager getTeleportManager() {
        return tm;
    }

    public CooldownManager getCooldownManager() {
        return cm;
    }

    public World getPlayWorld() {
        return this.getServer().getWorld(this.config.getWorldName());
    }

    public WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");

        if (!(plugin instanceof WorldGuardPlugin)) {
            return null;
        }

        return (WorldGuardPlugin) plugin;
    }
}