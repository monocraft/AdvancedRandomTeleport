package me.mrmaga.advancedrandomteleport.yml;

import me.mrmaga.advancedrandomteleport.AdvancedRandomTeleport;
import me.mrmaga.advancedrandomteleport.Utils;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class MainConfig extends CustomConfig {

    public MainConfig(AdvancedRandomTeleport main) {
        super(main, "config", true);
    }

    public String getPrefix() {
        return Utils.color(this.yml.getString("prefix"));
    }

    public boolean isCooldownEnabled() {
        try {
            return this.yml.getBoolean("cooldown-enabled");
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public boolean isWGEnabled() {
        try {
            return this.yml.getBoolean("enable-worldguard-cheks");
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public String getWorldName() {
        try {
            return this.yml.getString("world");
        } catch (NullPointerException npe) {
            return "world";
        }
    }

    public int getPreloadRadius() {
        return this.yml.getInt("preload-radius");
    }

    public List<String> getDisallowedRegions() {
        return this.yml.getStringList("disallowed-regions");
    }

    public List<String> getDisabledWorld() {
        return this.yml.getStringList("disabled-worlds");
    }

    public List<Material> getAvoidBlocks() {
        List<Material> list = new ArrayList<>();

        for (String type : this.yml.getStringList("avoid-blocks")) {
            list.add(Material.matchMaterial(type));
        }

        return list;
    }

    public long getCooldownTime(Player player) {
        long min = Long.MAX_VALUE;

        for (String group : this.yml.getConfigurationSection("groups-cooldown").getKeys(false)) {
            if (!player.hasPermission("art.cooldown." + group)) {
                continue;
            }

            long time = Utils.convertToMill(this.yml.getString("groups-cooldown." + group));

            if (time >= min) {
                continue;
            }

            min = time;
        }

        if (min == Long.MAX_VALUE) {
            min = Utils.convertToMill(this.yml.getString("standart-cooldown"));
        }

        return min;
    }
}
