package me.mrmaga.advancedrandomteleport.yml;

import me.mrmaga.advancedrandomteleport.AdvancedRandomTeleport;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class CustomConfig {
    protected YamlConfiguration yml;
    protected AdvancedRandomTeleport plugin;
    private final File file;

    public CustomConfig(AdvancedRandomTeleport plugin, String name, boolean isResource) {
        this.file = new File(plugin.getDataFolder(), name + ".yml");
        this.plugin = plugin;
        this.file.getParentFile().mkdirs();
        if (!this.file.exists() && isResource) {
            plugin.saveResource(name + ".yml", false);
        }
        this.yml = YamlConfiguration.loadConfiguration(file);
    }

    public FileConfiguration get() {
        return yml;
    }

    public void save() {
        try {
            this.yml.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        try {
            this.yml.load(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}