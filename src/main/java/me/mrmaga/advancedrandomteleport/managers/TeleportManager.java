package me.mrmaga.advancedrandomteleport.managers;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import io.papermc.lib.PaperLib;
import me.mrmaga.advancedrandomteleport.AdvancedRandomTeleport;
import me.mrmaga.advancedrandomteleport.Utils;
import me.mrmaga.advancedrandomteleport.yml.LanguageConfig;
import me.mrmaga.advancedrandomteleport.yml.MainConfig;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import xyz.tozymc.spigot.api.title.Title;
import xyz.tozymc.spigot.api.title.TitleApi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class TeleportManager {

    private final AdvancedRandomTeleport plugin;
    private final MainConfig config;
    private final LanguageConfig lang;

    public TeleportManager(AdvancedRandomTeleport plugin) {
        this.plugin = plugin;
        this.config = plugin.getMainConfig();
        this.lang = plugin.getLanguageConfig();
    }

    public void randomTeleport(Player player, boolean self) {
        Location spot = this.findRandomLocation(this.plugin.getPlayWorld());

        if (null == spot) {
            player.sendMessage(this.lang.getPrefixedMsg("cant-find-location"));
            return;
        }

        String subtitleText = ChatColor.translateAlternateColorCodes('&',
                this.lang.getMsg("find-location-subtitle"));
        String titleText = ChatColor.translateAlternateColorCodes('&',
                this.lang.getMsg("find-location-title"));
        TitleApi.sendTitle(player, new Title(titleText, subtitleText));

        Runnable runnable = () -> this.plugin.getServer().getScheduler().runTaskLater(this.plugin, () -> {
            String coords = spot.getBlockX() + " " + spot.getBlockY() + " " + spot.getBlockZ();
            String prefixedMsg = this.lang.getPrefixedMsg(self
                    ? "teleport-self"
                    : "teleported-by-other");

            player.teleport(spot);
            player.sendMessage(prefixedMsg.replace("%coords%", coords));

            if (self) {
                return;
            }

            String teleportedByOtherMessage = this.lang.getPrefixedMsg("you-teleported-other")
                    .replace("%player%", player.getDisplayName())
                    .replace("%coords%", coords);
            player.sendMessage(teleportedByOtherMessage);
        }, 30L );

        CompletableFuture.allOf(this.getChunksToLoad(spot).toArray(new CompletableFuture[]{})).thenRun(runnable);
    }

    private Location findRandomLocation(World world) {
        FileConfiguration cfg = config.get();

        int xMax = cfg.getInt("x-max");
        int xMin = cfg.getInt("x-min");
        int zMax = cfg.getInt("z-max");
        int zMin = cfg.getInt("z-min");

        Material material;

        int randomX;
        int randomY;
        int randomZ;

        int tries = 0;
        do {
            randomX = getRandom(xMin, xMax);
            randomZ = getRandom(zMin, zMax);

            randomY = world.getHighestBlockYAt(randomX, randomZ) - 1;
            material = world.getBlockAt(randomX, randomY, randomZ).getType();
        } while ((config.getAvoidBlocks().contains(material) || checkRegions(world, randomX, randomY + 1, randomZ)) && ++tries < 100);

        if (tries >= 100) {
            return null;
        }

        return new Location(world, (double) randomX + 0.5, (double) randomY + 1, (double) randomZ + 0.5);
    }

    private int getRandom(int min, int max) {
        return (int) (Math.random() * (double) (max - min + 1) + min);
    }

    private boolean checkRegions(World world, int x, int y, int z) {
        if (!this.config.isWGEnabled()) {
            return false;
        }

        WorldGuardPlugin wg = plugin.getWorldGuard();

        if (wg == null) {
            return false;
        }

        RegionManager manager = wg.getRegionManager(world);
        ApplicableRegionSet set = manager.getApplicableRegions(new Location(world, x, y, z));

        for (ProtectedRegion region : set) {
            if (Utils.containsIgnoreCase(config.getDisallowedRegions(), region.getId())) {
                return true;
            }
        }

        return false;
    }

    private List<CompletableFuture<Chunk>> getChunksToLoad(Location loc) {
        List<CompletableFuture<Chunk>> chunksToLoad = new ArrayList<>();
        int range = Math.round(Math.max(0, Math.min(16, config.getPreloadRadius())));

        for (int x = -range; x <= range; x++) {
            for (int z = -range; z <= range; z++) {
                double chunkX = loc.getX() + (x * 16);
                double chunkZ = loc.getZ() + (z * 16);

                Location locLoad = new Location(loc.getWorld(), chunkX, loc.getY(), chunkZ);
                CompletableFuture<Chunk> chunk = PaperLib.getChunkAtAsync(locLoad, true);

                chunksToLoad.add(chunk);
            }
        }

        return chunksToLoad;
    }

}
