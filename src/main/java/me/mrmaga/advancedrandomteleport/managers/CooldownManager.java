package me.mrmaga.advancedrandomteleport.managers;

import me.mrmaga.advancedrandomteleport.AdvancedRandomTeleport;
import me.mrmaga.advancedrandomteleport.yml.MainConfig;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class CooldownManager {

    private final MainConfig config;
    private final HashMap<String, Long> cd;

    public CooldownManager(AdvancedRandomTeleport main) {
        this.config = main.getMainConfig();
        this.cd = new HashMap<>();
    }

    public void addCooldown(Player player) {
        cd.put(player.getName().toLowerCase(), config.getCooldownTime(player) + System.currentTimeMillis());
    }

    public long getWaitTime(String name) {
        name = name.toLowerCase();

        if (!cd.containsKey(name)) {
            return -1;
        }

        return cd.get(name) - System.currentTimeMillis();
    }
}
